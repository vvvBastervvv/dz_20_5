// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ASnakeElementBase;
class AActor;
#ifdef MYPROJECT_SnakeBase_generated_h
#error "SnakeBase.generated.h already included, missing '#pragma once' in SnakeBase.h"
#endif
#define MYPROJECT_SnakeBase_generated_h

#define MyProject_Source_MyProject_SnakeBase_h_25_SPARSE_DATA
#define MyProject_Source_MyProject_SnakeBase_h_25_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSnakeElementOverlap);


#define MyProject_Source_MyProject_SnakeBase_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSnakeElementOverlap);


#define MyProject_Source_MyProject_SnakeBase_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeBase(); \
	friend struct Z_Construct_UClass_ASnakeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(ASnakeBase)


#define MyProject_Source_MyProject_SnakeBase_h_25_INCLASS \
private: \
	static void StaticRegisterNativesASnakeBase(); \
	friend struct Z_Construct_UClass_ASnakeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(ASnakeBase)


#define MyProject_Source_MyProject_SnakeBase_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeBase(ASnakeBase&&); \
	NO_API ASnakeBase(const ASnakeBase&); \
public:


#define MyProject_Source_MyProject_SnakeBase_h_25_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeBase(ASnakeBase&&); \
	NO_API ASnakeBase(const ASnakeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnakeBase)


#define MyProject_Source_MyProject_SnakeBase_h_25_PRIVATE_PROPERTY_OFFSET
#define MyProject_Source_MyProject_SnakeBase_h_22_PROLOG
#define MyProject_Source_MyProject_SnakeBase_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject_Source_MyProject_SnakeBase_h_25_PRIVATE_PROPERTY_OFFSET \
	MyProject_Source_MyProject_SnakeBase_h_25_SPARSE_DATA \
	MyProject_Source_MyProject_SnakeBase_h_25_RPC_WRAPPERS \
	MyProject_Source_MyProject_SnakeBase_h_25_INCLASS \
	MyProject_Source_MyProject_SnakeBase_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyProject_Source_MyProject_SnakeBase_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject_Source_MyProject_SnakeBase_h_25_PRIVATE_PROPERTY_OFFSET \
	MyProject_Source_MyProject_SnakeBase_h_25_SPARSE_DATA \
	MyProject_Source_MyProject_SnakeBase_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	MyProject_Source_MyProject_SnakeBase_h_25_INCLASS_NO_PURE_DECLS \
	MyProject_Source_MyProject_SnakeBase_h_25_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYPROJECT_API UClass* StaticClass<class ASnakeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyProject_Source_MyProject_SnakeBase_h


#define FOREACH_ENUM_EMOVEMENTDIRECTION(op) \
	op(EMovementDirection::UP) \
	op(EMovementDirection::DOWN) \
	op(EMovementDirection::RIGHT) \
	op(EMovementDirection::LEFT) 

enum class EMovementDirection;
template<> MYPROJECT_API UEnum* StaticEnum<EMovementDirection>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
