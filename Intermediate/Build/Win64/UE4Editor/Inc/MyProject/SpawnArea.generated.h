// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
#ifdef MYPROJECT_SpawnArea_generated_h
#error "SpawnArea.generated.h already included, missing '#pragma once' in SpawnArea.h"
#endif
#define MYPROJECT_SpawnArea_generated_h

#define MyProject_Source_MyProject_SpawnArea_h_15_SPARSE_DATA
#define MyProject_Source_MyProject_SpawnArea_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execRandomPoint);


#define MyProject_Source_MyProject_SpawnArea_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execRandomPoint);


#define MyProject_Source_MyProject_SpawnArea_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpawnArea(); \
	friend struct Z_Construct_UClass_ASpawnArea_Statics; \
public: \
	DECLARE_CLASS(ASpawnArea, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(ASpawnArea)


#define MyProject_Source_MyProject_SpawnArea_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASpawnArea(); \
	friend struct Z_Construct_UClass_ASpawnArea_Statics; \
public: \
	DECLARE_CLASS(ASpawnArea, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(ASpawnArea)


#define MyProject_Source_MyProject_SpawnArea_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpawnArea(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpawnArea) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawnArea); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawnArea); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawnArea(ASpawnArea&&); \
	NO_API ASpawnArea(const ASpawnArea&); \
public:


#define MyProject_Source_MyProject_SpawnArea_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawnArea(ASpawnArea&&); \
	NO_API ASpawnArea(const ASpawnArea&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawnArea); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawnArea); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpawnArea)


#define MyProject_Source_MyProject_SpawnArea_h_15_PRIVATE_PROPERTY_OFFSET
#define MyProject_Source_MyProject_SpawnArea_h_12_PROLOG
#define MyProject_Source_MyProject_SpawnArea_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject_Source_MyProject_SpawnArea_h_15_PRIVATE_PROPERTY_OFFSET \
	MyProject_Source_MyProject_SpawnArea_h_15_SPARSE_DATA \
	MyProject_Source_MyProject_SpawnArea_h_15_RPC_WRAPPERS \
	MyProject_Source_MyProject_SpawnArea_h_15_INCLASS \
	MyProject_Source_MyProject_SpawnArea_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyProject_Source_MyProject_SpawnArea_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject_Source_MyProject_SpawnArea_h_15_PRIVATE_PROPERTY_OFFSET \
	MyProject_Source_MyProject_SpawnArea_h_15_SPARSE_DATA \
	MyProject_Source_MyProject_SpawnArea_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	MyProject_Source_MyProject_SpawnArea_h_15_INCLASS_NO_PURE_DECLS \
	MyProject_Source_MyProject_SpawnArea_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYPROJECT_API UClass* StaticClass<class ASpawnArea>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyProject_Source_MyProject_SpawnArea_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
